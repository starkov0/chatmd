/* Modele correspondant a un user loggué. Syntaxiquement identique a un User normal */

window.AuthUser = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "auth",

	initialize: function() {
		this.fetch();
	},

	defaults: {
		id: null,
		name:  "",
		pwd: ""
	}


});
