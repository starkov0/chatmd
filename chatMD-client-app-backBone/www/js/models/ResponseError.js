/* Error messages recieved from the server */
window.ResponseError = Backbone.Model.extend({
	defaults: {
		message: ''
	}
});
