/* Modele correspondant a des Conversations  */
window.Conversation = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "conversation",

	defaults: {
		title: '',
		crypto_key: ''
	},

	link_to_me: function() {
		return '#/conversation/' + this.get('id');
	}
});

