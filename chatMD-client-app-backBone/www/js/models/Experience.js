/* Modele correspondant a une Experience  */

window.ExperienceURL = window.RestUrlPrefix + "experience/";

window.Experience = Backbone.Model.extend({
	urlRoot: window.ExperienceURL,

	defaults: {
		id: null,
		title:  "",
		description:  "",
		begin: null,
		end: null,
		user_id: null
	}
});

/**
 *  Les experiences owned par un user.
 */
window.ExperienceCollection = Backbone.Collection.extend({
	model: Experience,
	initialize: function(properties) {
		this.userid = properties.user.get('id');
		this.fetch();
	},

	url: function() {
		return window.ExperienceURL  + 'user/' + this.userid;
	}
});

