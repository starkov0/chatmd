/* Modele correspondant a un ConversationMsg */

window.ConversationMsgURL = window.RestUrlPrefix + "conversation_msg/";

window.ConversationMsg = Backbone.Model.extend({
	urlRoot: window.ConversationMsgURL,

	defaults: {
		id: null,
		user_id: null,
		conversation_id: null,
		content: ''
	}
});

window.ConversationMsg_ofConversation = Backbone.Collection.extend({
	model: ConversationMsg,
	initialize: function(prop) {
		this.conversation_id = prop.conversation.get('id');
	},

	url: function() {
		return window.ConversationMsgURL +
		       'conversation/' + this.conversation_id;
	}
});

window.ConversationMsg_LastofConversation = Backbone.Collection.extend({
	model: ConversationMsg,
	initialize: function(prop) {
		this.conversation_id = prop.conversation.get('id');
	},

	url: function() {
		return window.ConversationMsgURL +
		       'last/conversation/' + this.conversation_id;
	}
});

