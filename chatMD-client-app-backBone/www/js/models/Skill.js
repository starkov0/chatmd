/* Modele correspondant a un Skill  */

window.SkillURL = window.RestUrlPrefix + "skill/";

window.Skill = Backbone.Model.extend({
	urlRoot: window.SkillURL,
	
	defaults: {
		id: null,
		title:  "",
		description:  "",
		user_id: null
	}
});

/**
 *  Les skills owned par un user.
 */
window.SkillCollection = Backbone.Collection.extend({
	model: Skill,
	initialize: function(properties) {
		this.userid = properties.user.get('id');
		this.fetch()
	},

	url: function() {
		return window.SkillURL + 'user/' + this.userid;
	}
});

