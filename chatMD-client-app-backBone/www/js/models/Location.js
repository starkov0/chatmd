/* Modele correspondant a des Users au sens GMP */
window.Location = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "location/user/",

	initialize: function(properties) {
		this.user = properties.user;
		this.urlRoot += this.user.get('id');
		this.fetch()
	},

	defaults: {
		id: null,
		name: '',
		latitude: null,
		longitude: null,
		userid: null
	}
});
