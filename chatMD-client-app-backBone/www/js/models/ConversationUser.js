/* Modele correspondant a un ConversationUser */

window.ConversationUserURL = window.RestUrlPrefix + "conversation_user/";

window.ConversationUser = Backbone.Model.extend({
	urlRoot: window.ConversationUserURL,

	defaults: {
		user_id: null,
		conversation_id: null,
		crypto_key_pwd: ''
	}
});

window.ConversationUser_ofConversation = Backbone.Collection.extend({
	model: ConversationUser,
	initialize: function(prop) {
		this.conversation_id = prop.conversation.get('id');
	},

	url: function() {
		return window.ConversationUserURL +
		       'conversation/' + this.conversation_id;
	}
});


window.ConversationUser_ofUser = Backbone.Collection.extend({
	model: ConversationUser,
	initialize: function(prop) {
		this.user_id = prop.user.get('id');
	},

	url: function() {
		return window.ConversationUserURL + 'user/' + this.user_id;
	}
});
