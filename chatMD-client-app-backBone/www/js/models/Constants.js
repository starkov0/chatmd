/* Les constantes de GMP, prises depuis un service */

window.ConstantsModel = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "constants",
	defaults: {
		NAME_LEN: 50,
		COMMENT_LEN: 500,
		GEAR_TITLE_LEN: 40,
		GEAR_TITLE_LEN_MIN: 3,
		TEL_LEN: 15,
		EMAIL_LEN: 100,
		GEAR_DESC_MAX_LEN: 500,
		ACTIVATION_HASH_LEN :40,
		QR_LEN_MIN: 4,
		USERNAME_MIN_LEN: 4
	}
});
