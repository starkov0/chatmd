/* Modele correspondant a des Users au sens GMP */
window.User = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "user",

	defaults: {
		email:'',
		name: '',
		title: '',
		abstract:'',
		hobbies: '',
		image: '',
		profile_pic: ''
	},

	is_myself: function() {
		return this.get('id') == window.LoggedUser.get('id');
	},

	link_to_me: function() {
		return '#/user/' + this.get('id');
	}
});
