/* ROUTEUR DE L'APPLICATION */
var AppRouter = Backbone.Router.extend({

	initialize: function() {
		//Initialize the publish/subscribe bus
		window.pubSubBus = _.extend({},Backbone.Events);
		window.LoggedUser = new AuthUser();

		window.header_title = "What'sMD";
		window.header_left_arrow_display = false;
		window.header_left_arrow_path = '#/';

		new HeaderView().render();
		new MenuView().render();

		this.actualPage = '';
		this.bind('route', this.pageChange);
	},

	routes: {
		'' : 'index',
		//USER
		"user/:id" : "user_view",

		//USERS
		"users/search" : "users_search",

		//UPDATE
		"update/abstract" : "update_abstract",
		"update/hobbies" : "update_hobbies",
		"update/name" : "update_name",
		"update/title" : "update_title",


		//CONVERSATIONS
		"conversations" : "conversations_view",

		//CONVERSATION
		"conversation/:id" : "conversation_view"
	},

	index: function() {
		app.showViewContent(new IndexView());
	},

	/* USER */
	user_view: function(userid) {
		var user = new User({id: userid});
		user.fetch({
			success: function() {
				app.showViewContent( new UserViewView({user: user}));
			}
		});
	},

	/* USERS */
	users_search: function() {
		app.showViewContent( new UsersSearchView());
	},

	/* UPDATE */
	update_abstract: function() {
		var user = new User({id: 1});
		user.fetch({
			success: function() {
				app.showViewContent( new UpdateAbstractViewView({user: user}));
			}
		});
	},
	update_hobbies: function() {
		var user = new User({id: 1});
		user.fetch({
			success: function() {
				app.showViewContent( new UpdateHobbiesViewView({user: user}));
			}
		});
	},
	update_name: function() {
		var user = new User({id: 1});
		user.fetch({
			success: function() {
				app.showViewContent( new UpdateNameViewView({user: user}));
			}
		});
	},
	update_title: function() {
		var user = new User({id: 1});
		user.fetch({
			success: function() {
				app.showViewContent( new UpdateTitleViewView({user: user}));
			}
		});
	},

	/* CONVERSATIONS */
	conversations_view: function() {
		app.showViewContent(new ConversationsViewView());
	},

	/* CONVERSATION */
	conversation_view: function(id) {
		var conversation = new Conversation({id: id});
		conversation.fetch({
			success: function() {
				app.showViewContent(new ConversationViewView({conversation: conversation}));
			}
		});
	},

	showViewContent: function(view) {
		if (this.currentViewContent)
			this.currentViewContent.close(); //on detatch l'ancienne view

		$('#content').html(view.render().el); //Affiche la nouvelle
		this.currentViewContent = view;
		return view;
	},

	pageChange: function(view) {
		console.log('Called: '+this.actualPage);
		window.oldPage = this.actualPage;
		this.actualPage = Backbone.history.getFragment();
	}

});

function ChangePageTitle(newTitle, left_arrow_display) {

	left_arrow_display = typeof left_arrow_display !== 'undefined' ?
		left_arrow_display : false;

	//Change the title
	window.header_title = newTitle;
	window.header_left_arrow_display = left_arrow_display;
	window.header_left_arrow_path = '#/' + window.oldPage;
	window.pubSubBus.trigger('header:title:change');
};


