/* Affiche les details d'un message */
window.MessageViewView = Backbone.View.extend({

	tagName: 'li',
	className: 'table-view-cell media',

	initialize: function(args) {
		this.template = _.template(tpl.get('message/view'));

		this.message = args.message;
		this.user = new User({id: this.message.get('user_id')});
		this.listenTo(this.user, 'sync', this.render);
		this.user.fetch({cache: true});
	},


	render: function() {
		//On affiche la page
		$(this.el).html(this.template({
			message: this.message,
			user: this.user
		}));

		return this;
	}
});
