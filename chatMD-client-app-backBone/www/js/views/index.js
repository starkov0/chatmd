window.IndexView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('index'));
	},

	render: function(e) {
		$(this.el).html(this.template({}));
		return this;
	}
});
