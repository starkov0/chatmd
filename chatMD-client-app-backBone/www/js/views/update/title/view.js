/* Update le titre d'un user */
window.UpdateTitleViewView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('update/title/view'));
		this.user = args.user;
	},

	events: {
		"click #update": "update",
		"click #delete": "delete"
	},

	update: function () {
		var form = $(this.el).find('form#account');
		this.user.set("title", form.find('#title').val());
		console.log(form.find('#title').val());

		this.user.save();
		app.navigate(this.user.link_to_me(), {trigger: true})
	},

	delete: function() {
		this.user.set("title", "");
		this.user.save();
		app.navigate(this.user.link_to_me(), {trigger: true})
	},

	render: function() {
		ChangePageTitle("Change Title", true);

		$(this.el).html(this.template({
			user: this.user
		}));

		return this;
	}
});
