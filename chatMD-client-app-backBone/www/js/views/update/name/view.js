/* Update le titre d'un user */
window.UpdateNameViewView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('update/name/view'));
		this.user = args.user;
	},

	events: {
		"click #update": "update",
	},

	update: function () {
		var form = $(this.el).find('form#account');
		this.user.set("name", form.find('#name').val());
		this.user.save()
		app.navigate(this.user.link_to_me(), {trigger: true})
	},

	render: function() {
		ChangePageTitle("Change Name", true);

		$(this.el).html(this.template({
			user: this.user
		}));

		return this;
	}
});
