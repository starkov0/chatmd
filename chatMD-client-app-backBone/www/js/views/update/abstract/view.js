/* Update le titre d'un user */
window.UpdateAbstractViewView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('update/abstract/view'));
		this.user = args.user;
	},

	events: {
		"click #update": "update",
		"click #delete": "delete"
	},

	update: function () {
		var form = $(this.el).find('form#account');
		this.user.set("abstract", form.find('#abstract').val());
		this.user.save()
		app.navigate(this.user.link_to_me(), {trigger: true})
	},

	delete: function() {
		this.user.set("abstract", "");
		this.user.save()
		app.navigate(this.user.link_to_me(), {trigger: true})
	},

	render: function() {
		ChangePageTitle("Change Abstract", true);

		$(this.el).html(this.template({
			user: this.user
		}));

		return this;
	}
});
