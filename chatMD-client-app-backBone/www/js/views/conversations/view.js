/* Affiche les conversations d'un User */
window.ConversationsViewView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('conversations/view'));

		this.conversationUsers = new ConversationUser_ofUser({user: window.LoggedUser});
		this.listenTo(this.conversationUsers, 'sync', this.render_conversations);
	},


	render: function() {
		ChangePageTitle("Conversations");

		this.conversationUsers.fetch();

		//On affiche la page
		$(this.el).html(this.template());

		return this;
	},

	render_conversations: function() {
		console.log(this.conversationUsers);
		this.conversationUsers.each(function(CU) {
			this.$('#all_conversations').append(
				new ConversationsViewConversationView({
					conversationUser: CU,
					user: this.user
				}).render().$el
			);
		}, this);
	}
});
