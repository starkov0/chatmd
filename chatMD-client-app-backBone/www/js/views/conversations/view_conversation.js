var ConversationsViewConversationView = Backbone.View.extend({

	tagName: 'li',
	className: 'table-view-cell media',

	initialize: function(attrs) {
		this.tpl = _.template(tpl.get('conversations/view_conversation'));
		this.user = attrs.user;
		this.conversationUser = attrs.conversationUser;

		this.conversation = new Conversation({
			id: this.conversationUser.get('conversation_id')});

		this.CUparticipants = new ConversationUser_ofConversation({
			conversation: this.conversation});
		this.CUparticipants_fetched = false;

		this.last_msg = new ConversationMsg_LastofConversation({
			conversation: this.conversation});



		this.listenTo(this.conversation, 'sync', this.render);
		this.listenTo(this.CUparticipants, 'sync', this.render);
		this.listenTo(this.last_msg, 'sync', this.render);

		this.last_msg.fetch();

		this.conversation.fetch();
	},

	render: function() {


		this.$el.html(this.tpl({
			user: this.user,
			conversation: this.conversation,
			last_msg: this.last_msg.at(0)
		}));

		this.CUparticipants.each(function(CU) {
			this.$('ul').append(
				new UserMiniviewView({
					userid: CU.get('user_id')
				}).render().$el);
		}, this);

		if(!this.CUparticipants_fetched) {
			this.CUparticipants.fetch();
			this.CUparticipants_fetched = true;
		}


		return this;
	}
});

