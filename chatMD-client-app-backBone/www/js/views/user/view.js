/* Affiche les details d'un user */
window.UserViewView = Backbone.View.extend({
	
	initialize: function(args) {
		this.template = _.template(tpl.get('user/view'));

		this.user = args.user;
		this.location = new Location({user: this.user});
		this.skills = new SkillCollection({user: this.user});
		this.experiences = new ExperienceCollection({user: this.user});

		this.listenTo(this.user, 'sync', this.render);
		this.listenTo(this.skills, 'sync', this.render_skills);
		this.listenTo(this.experiences, 'sync', this.render_experiences);
		this.listenTo(this.location, 'sync', this.render_location);
	},

	render: function() {

		ChangePageTitle(this.user.get('name'), true);

		//On affiche la page
		$(this.el).html(this.template({
			user: this.user
		}));

		return this;
	},

	render_skills: function() {

		//Puis les skills
		this.skills.each(function(skill) {
			this.$('#all_skills').append(
				new SkillViewView({
					skill: skill,
					user: this.user
				}).render().$el
			);
		}, this);
	},

	render_experiences: function() {

		//Puis les expériences
		this.experiences.each(function(experience) {
			this.$('#all_experiences').append(
				new ExperienceViewView({
					experience: experience,
					user: this.user
				}).render().$el
			);
		}, this);
	},

	render_location: function() {

		//Puis la location
		this.$('#location').append(
			new LocationViewView({
				location: this.location,
				user: this.user
			}).render().$el
		);
	}
});
