/* Affiche les details d'un user */
window.UserMiniviewView = Backbone.View.extend({
	
	tagName: 'li',
	className: 'table-view-cell',

	initialize: function(args) {
		this.template = _.template(tpl.get('user/miniview'));

		this.userid = args.userid;
		this.user = new User({id: this.userid});
		this.listenTo(this.user, 'sync', this.render);
		this.user.fetch({cache: true});
	},


	render: function() {
		//On affiche la page
		$(this.el).html(this.template({
			user: this.user,
		}));

		return this;
	}
});
