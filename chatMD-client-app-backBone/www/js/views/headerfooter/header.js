/* Affiche le header de GMP */
window.HeaderView = Backbone.View.extend({

	el: '#header',

	//On a un UserAuth comme modele
	initialize: function() {
		this.template = _.template(tpl.get('headerfooter/header'));
		this.listenTo(window.LoggedUser, 'sync', this.render);

		this.listenTo(window.pubSubBus, 'header:title:change', this.render);
	},

	render: function() {
		$(this.el).html(this.template({
			LoggedUser: window.LoggedUser,
			title: window.header_title,
			header_left_arrow_display: window.header_left_arrow_display,
			header_left_arrow_path: window.header_left_arrow_path
		}));
		return this;
	}
});
