/* Affiche le menu de GMP */
window.MenuView = Backbone.View.extend({

	el: '#menu',

	//On a un UserAuth comme modele
	initialize: function() {
		this.template = _.template(tpl.get('headerfooter/menu'));
		this.listenTo(window.LoggedUser, 'sync', this.render);
	},

	render: function() {
		$(this.el).html(this.template({
			LoggedUser: window.LoggedUser
		}));
		return this;
	}
});
