var SkillViewView = Backbone.View.extend({

	tagName: 'li',
	className: 'table-view-cell media',

	initialize: function(attrs) {
		this.tpl = _.template(tpl.get('skill/view'));
		this.user = attrs.user;
		this.skill = attrs.skill;
	},

	render: function() {
		this.$el.html(this.tpl({
			skill: this.skill,
			user: this.user
		}));

		return this;
	}
});

