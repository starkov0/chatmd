var LocationViewView = Backbone.View.extend({

	initialize: function(attrs) {
		this.tpl = _.template(tpl.get('location/view'));
		this.user = attrs.user;
		this.location = attrs.location;
	},

	render: function() {

		this.$el.html(this.tpl({
			location: this.location,
			user: this.user
		}));

		return this;
	}
});

