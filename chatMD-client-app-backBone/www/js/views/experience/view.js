var ExperienceViewView = Backbone.View.extend({

	tagName: 'li',
	className: 'table-view-cell media',

	initialize: function(attrs) {
		this.tpl = _.template(tpl.get('experience/view'));
		this.user = attrs.user;
		this.experience = attrs.experience;
	},

	render: function() {
		this.$el.html(this.tpl({
			experience: this.experience,
			user: this.user
		}));

		return this;
	}
});

