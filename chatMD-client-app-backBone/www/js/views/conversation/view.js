/* Affiche les conversations d'un User */
window.ConversationViewView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('conversation/view'));

		this.conversation = args.conversation;

		this.CUparticipants = new ConversationUser_ofConversation({
			conversation: this.conversation});

		this.messages = new ConversationMsg_ofConversation({
			conversation: this.conversation});

		this.listenTo(this.CUparticipants, 'sync', this.render);
		this.listenTo(this.messages, 'sync', this.render);

		this.CUparticipants.fetch();
		this.messages.fetch();
	},


	render: function() {
		ChangePageTitle(this.conversation.get('title'), true);

		//On affiche la page
		$(this.el).html(this.template({
			conversation: this.conversation
		}));


		this.CUparticipants.each(function(CU) {
			this.$('ul.participants').append(
				new UserMiniviewView({
					userid: CU.get('user_id')
				}).render().$el);
		}, this);

		this.messages.each(function(msg) {
			this.$('ul.messages').append(
				new MessageViewView({
					message: msg
				}).render().$el);
		}, this);

		return this;
	}
});

