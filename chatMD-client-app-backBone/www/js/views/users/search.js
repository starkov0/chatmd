/* Affiche les details d'un user */
window.UsersSearchView = Backbone.View.extend({

	initialize: function(args) {
		this.template = _.template(tpl.get('users/search'));
	},


	render: function() {
		var that = this;

		ChangePageTitle("Recherche");

		$(that.el).html(that.template({}));

		return this;
	}
});
