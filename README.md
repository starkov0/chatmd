### CHATMD ###

* ChatMD is a chat app for medical purpose.
It will allow physicians communicate efficiently via messaging while keeping their conversations encrypted with the use of RSA and AES algorithms.
* ChatMD is developed on Ionic, which encapsulates AngularJS.
* Realtime-chat is a temporary app for testing Ionic capabilities.